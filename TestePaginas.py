from bs4 import BeautifulSoup
from selenium import webdriver

cat = 'perfumes'
my_url = 'http://www.epocacosmeticos.com.br/' + cat + '#'

driver = webdriver.PhantomJS()
driver.get(my_url)
SoupJs = BeautifulSoup(driver.page_source, "html.parser")
produtos = SoupJs.findAll("div", {"class": "shelf-default__item skip"})

i = 1
for produto1 in produtos:
    print(str(i) + ' - ' + produto1['title'])

while produtos is not None:

    i = i + 1
    driver.refresh()
    driver.get(my_url + str(i))

    SoupJs = BeautifulSoup(driver.page_source, "html.parser")
    produtos = SoupJs.findAll("div", {"class": "shelf-default__item skip"})

    for produto2 in produtos:

        print(str(i) + ' - ' + produto2['title'])
