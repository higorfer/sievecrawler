import asyncio
from aiohttp import ClientSession


urls = (
    'http://www.epocacosmeticos.com.br/perfumes',
    'http://www.epocacosmeticos.com.br/cabelos',
    'http://www.epocacosmeticos.com.br/maquiagem',
    'http://www.epocacosmeticos.com.br/dermocosmeticos',
)


async def get_and_print(session, url):
    async with session.get(url) as response:
        print(await response.text())


async def fetch(loop, urls):
    async with ClientSession(loop=loop) as session:
        tasks = tuple(get_and_print(session, url) for url in urls)
        await asyncio.gather(*tasks, return_exceptions=True)


loop = asyncio.get_event_loop()
loop.run_until_complete(fetch(loop, urls))
