from selenium import webdriver
from bs4 import BeautifulSoup
import time


cat = 'perfumes'
my_url = 'http://www.epocacosmeticos.com.br/' + cat + '#'

driver = webdriver.PhantomJS()
driver.get(my_url)

soup = BeautifulSoup(driver.page_source, "html.parser")

produtos = soup.findAll("div", {"class": "shelf-default__item skip"})
prateleira = soup.find("div", {"class": "shelf-default prateleira n4colunas"})
pagina = soup.find("li", {"class": "page-number pgCurrent"})

for produto in produtos:
    nome = produto['title']
    url_produto = produto.a['href']
    print(str(pagina) + ' - ' + nome + ' - ' + url_produto)


while prateleira is not None:

    driver.find_elements_by_class_name('next')[1].click()

    time.sleep(1)
    soup = BeautifulSoup(driver.page_source, "html.parser")
    produtos = soup.findAll("div", {"class": "shelf-default__item skip"})
    prateleira = soup.find("div", {"class": "shelf-default prateleira n4colunas"})
    pagina = soup.find("li", {"class": "page-number pgCurrent"})

    for produto in produtos:

        nome = produto['title']
        url_produto = produto.a['href']
        print(str(pagina) + ' - ' + nome + ' - ' + url_produto)
