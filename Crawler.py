from bs4 import BeautifulSoup
from selenium import webdriver
from time import sleep


def write_line_csv(nome, url_produto, titulo):
    # arquivo csv
    filename = "produtos.csv"
    with open(filename, "w") as f:
        try:
            linha = (nome + ' , ' + url_produto + ' , ' + titulo + '\n')
            f.write(linha)
        except IOError:
            print("Could not read file:", filename)
        finally:
            f.close()


def geturlcategorias(url):

    print('geturl')
    driver = webdriver.PhantomJS()
    driver.get(url)
    try:
        soup = BeautifulSoup(driver.page_source, "html.parser")
        categorias = soup.find("ul", {"class": "submenu__list"})
        c = []
        for cat in categorias:
            c.append(cat.a['href'])
        return c
    except Exception as e:
        return str(e)


def parse_paginas(cat):
    print('parsepaginas')

    driver = webdriver.PhantomJS()
    driver.refresh()
    driver.get(cat)

    soup_pagina1 = BeautifulSoup(driver.page_source, "html.parser")
    produtos = soup_pagina1.findAll("div", {"class": "shelf-default__item skip"})
    prateleira = soup_pagina1.find("div", {"class": "shelf-default prateleira n4colunas"})

    i = 1
    print('pagina 1')

    # primeira pagina
    for produto in produtos:

        nome = produto['title']
        url_produto = produto.a['href']

        # gargalo para obter o titulo do produto
        driverproduto = webdriver.PhantomJS()
        driverproduto.get(url_produto)
        produtosoup = BeautifulSoup(driverproduto.page_source, "html.parser")
        nome_produto = produtosoup.find("div", {"class": "product__floating-info--name"}).text.strip()
        driverproduto.close()

        print('pagina - ' + str(i) + ' - ' + nome + ' - ' + url_produto + ' - ' + nome_produto)
        write_line_csv(nome, url_produto, nome_produto)

    # percorre paginas
    while prateleira is not None:

        i += 1
        print('pagina ' + str(i))
        driver.find_elements_by_class_name('next')[1].click()
        driver.refresh()
        sleep(2)

        soup_pagina = BeautifulSoup(driver.page_source, "html.parser")
        produtos = soup_pagina.findAll("a", {"class": "shelf-default__product-name"})
        prateleira = soup_pagina.find("div", {"class": "shelf-default prateleira n4colunas"})

        # percorre produtos nas paginas
        for produto in produtos:

            nome = produto['title']
            url_produto = produto.a['href']

            driverproduto = webdriver.PhantomJS()
            driverproduto.get(url_produto)
            produtosoup = BeautifulSoup(driverproduto.page_source, "html.parser")

            nome_produto = produtosoup.find("div", {"class": "product__floating-info--name"}).text.strip()
            driverproduto.close()

            print('pagina - ' + str(i) + ' - ' + nome + ' - ' + url_produto + ' - ' + nome_produto)
            write_line_csv(nome, url_produto, nome_produto)

    driver.close()


myurl = 'http://www.epocacosmeticos.com.br/'
# csv headers
write_line_csv('Nome', 'URL', 'Titulo')

for categoria in geturlcategorias(myurl):

    # salva categoria
    parse_paginas(categoria)
